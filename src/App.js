import './App.css';
import React, { Component } from 'react';
import Addlist from './components/Addlist';
import Search from './components/Search';
import Sort from './components/Sort';
import Table from './components/Table';
// import { useState, useEffect } from 'react'

export default class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      tasklist : [],
      stateAddliste : false,
      task_edit : "",
      filter : {
        filter_name : "",
        filter_status : Boolean
      }
    };
    this.addlist = this.addlist.bind(this)
    this.changestyle = this.changestyle.bind(this)
    this.SaveValue = this.SaveValue.bind(this)
    this.ChangeStatus = this.ChangeStatus.bind(this)
    this.remove = this.remove.bind(this)
    this.edit = this.edit.bind(this)
    this.filter_status = this.filter_status.bind(this)
  }

  check_name_filter(param,value){
    if(param === "name"){
      this.setState(prevState => ({
        filter: {                  
          ...prevState.filter,    
          filter_name: value       
        }
      }))
    }
  }

  filter_status(e){
    let name = e.target.name;
    let value = e.target.value;
    if(name === "status" && value === "Active"){
      this.setState(prevState => ({
        filter: {                  
          ...prevState.filter,    
          filter_status: true       
        }
      }))
      this.check_name_filter(name,value);
    }else if(name === "status" && value === "No active"){
      this.setState(prevState => ({
        filter: {                  
          ...prevState.filter,    
          filter_status: false       
        }
      }))
      this.check_name_filter(name,value);
    }else if(name === "status" && value === "All status"){
      this.setState(prevState => ({
        filter: {                  
          ...prevState.filterfilter,    
          filter_status: ""       
        }
      }))
      this.check_name_filter(name,value);
    }else if(name === "name"){
      this.check_name_filter(name,value);
    }
    
  }

  addlist(){
    this.setState({stateAddliste: true,task_edit:""})
  }

  changestyle(){
    this.setState({stateAddliste: !this.state.stateAddliste})
  }

  SaveValue(e){
    let filteredItems;
    let arr_id = [];
    this.state.tasklist.map((tasklist, index) => {
      if(tasklist.id === e.id){
        filteredItems = this.state.tasklist.filter(item => item !== tasklist)
      }
      arr_id.push(tasklist.id)
    })
    let check_id = arr_id.includes(e.id)
    if(check_id === true){
      this.setState({tasklist: [...filteredItems, e]})
      console.log("true")
    }else{
      this.setState({
        tasklist : [
          ...this.state.tasklist,
          e
        ]
      })
    }
  }

  componentWillMount() {
    if(localStorage && localStorage.getItem("task")){
      this.setState({tasklist: JSON.parse(localStorage.getItem("task"))})
    }
  }

  ChangeStatus(e){
    this.state.tasklist.map((task,index) => {
      if(task.id === e){
        task.status = !task.status
      }
    })
    this.setState({
      tasklist: this.state.tasklist
    })
  }

  remove(e){
    let filteredItems;
    this.state.tasklist.map((task,index) => {
      if(task.id === e){
        filteredItems = this.state.tasklist.filter(item => item !== task)
      }
    })
    this.setState({tasklist: filteredItems})
  }
  
  edit(e){
    this.setState({stateAddliste:true})
    this.state.tasklist.map((task,index) => {
      if(task.id === e){  
        this.setState({task_edit: task})
      }
    })
  }

  render() {
    localStorage.setItem("task", JSON.stringify(this.state.tasklist));

    let { stateAddliste, task_edit,tasklist  } = this.state;
    let element_addlist = stateAddliste === true ? <Addlist task = {stateAddliste === true ? task_edit : ""} style={this.changestyle} SaveValue = {this.SaveValue}/> : "";

    // let filter_name =  this.state.filter.filter_name
    // if(this.state.filter.filter_name){
    //   tasklist.map((value,index) =>{
    //     if(value.name_data.includes(filter_name)){
    //       console.log(value.name_data)
    //     }
   
    //   })
    // }
    return (
      <div className="container">
        <div className="row">
          {element_addlist}
          <div className ="col">
            <button className="btn btn-primary" onClick={this.addlist}>Add list</button>
            <div className="row">
             <Search />
             <Sort />
            </div>
            <Table 
            taskList = {tasklist}
            status = {this.ChangeStatus}
            remove = {this.remove}
            edit = {this.edit}
            filter = {this.state.filter}
            filter_status = {this.filter_status}
            />
          </div>
        </div>
      </div>
    )
  }
}

// export default class App extends Component {
//   constructor(props){
//     super(props)
//     this.state ={
//       username : "",
//       password: "",
//       des: "",
//       select: 0,
//       rd: "no",
//       check: true
//     }
//     this.get_value = this.get_value.bind(this)
//     this.save = this.save.bind(this)
//   }

//   save(e){
//     e.preventDefault()
//     console.log(this.state)
//   }

//   get_value(value){
//     if(value.target.type === "checkbox"){
//       this.setState({
//         [value.target.name]:value.target.checked
//       })
//     }else{
//       this.setState({
//         [value.target.name]:value.target.value
//       })
//     }
//   }
//   render() {
//     return (
//       <div className="container">
//         <form className="form-control">
//           <label>Username:</label><br />
//           <input className="form-control" type="text" name="username" onChange={this.get_value} value={this.state.username} /><br />
//           <label >Password:</label><br />
//           <input className="form-control" type="password" name="password" onChange={this.get_value} value={this.state.password}/><br />
//           <label >Description:</label><br />
//           <textarea className="d-block mg-15 form-control" type="text" name="des" onChange={this.get_value} value={this.state.des}></textarea><br />
//           <label >Gioi tinh:</label><br />
//           <select className="d-block mg-15 form-control" value={this.state.select} onChange={this.get_value} name ="select">
//             <option value={0}>Nam</option>
//             <option value={1}>Nữ</option>
//           </select>
//           <label >Yes or No</label><br />
//           <div className="radio"  >
//             <label>
//               <input checked ={this.state.rd === "yes"} value="yes" name="rd" type="radio" onChange={this.get_value}/> Yes
//             </label>
//             <label>
//               <input checked ={this.state.rd === "no"}  value="no" name="rd" type="radio" onChange={this.get_value}/> No
//             </label>
//           </div>

//           <div className="radio">
//             <input type="checkbox" name="check" checked={this.state.check === true} onChange={this.get_value}/>
//             Active
//           </div>
//           <div className="form-check">
//             <label className="form-check-label">
//               <input type="checkbox" className="form-check-input" name="" id="" value="checkedValue" />
//               Display value
//             </label>
//           </div>
//           <button className="btn" type="submit" onClick={this.save}>Save</button>
//           <button className="btn" type="reset" >Reset</button>
//         </form>
//       </div>
//     )
//   }
// }
