import React, { Component } from 'react'

export default class TaskItem extends Component {
    render() {
        let filter_con = this.props.filter_con;
        let element = Array;
        if(this.props.taskItem !== ""){
            element =  
            this.props.taskItem.map((taskItem, index)=>{
                return(
                    <tr id={taskItem.id} key={index} style={filter_con.filter_status === taskItem.status || filter_con.filter_status === "" || filter_con.filter_status === Boolean || taskItem.name_data.includes(filter_con.filter_name) || filter_con.filter_name === "" ? {display:"table-row"} : {display:"none"}}>
                        <td>{index}</td>
                        <td>{taskItem.name_data}</td>
                        <td onClick={() => {this.props.status_con(taskItem.id)}}>{taskItem.status === true ? "Active" : "No Active"}</td>
                        <td>
                            <button className="btn btn-primary" onClick={() => {this.props.edit_con(taskItem.id)}}>Edit</button>
                            <button className="btn btn-danger" onClick={() => {this.props.remove_con(taskItem.id)}}>Remove</button>
                        </td>
                    </tr>
                    
                )
            })
        }
        return(element)  
    }
}
