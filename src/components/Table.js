import React, { Component } from 'react'
import TaskItem from './TaskItem';

export default class Table extends Component { 
    render() {
        
        return (
            <div>
                <table className="table">
                    <thead>
                        <tr>
                        <th>STT</th>
                        <th>Name</th>
                        <th>State</th>
                        <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td></td>
                            <td>
                                <input
                                className="form-control"
                                name="name" 
                                onChange={this.props.filter_status}
                                />
                            </td>
                            <td>
                                <select className="form-control" name="status" onChange={this.props.filter_status}>
                                    <option>All status</option>
                                    <option>Active</option>
                                    <option>No active</option>
                                </select>
                            </td>
                            <td></td>
                        </tr>
                        <TaskItem
                        taskItem = {this.props.taskList}
                        status_con = {this.props.status}
                        remove_con = {this.props.remove}
                        edit_con = {this.props.edit}
                        filter_con = {this.props.filter}
                        /> 
                    </tbody>
                </table>
            </div>
        )
    }
}
