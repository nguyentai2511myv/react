import React, { Component } from 'react'

export default class Search extends Component {
    render() {
        return (
            <div className="d-flex col-6 height-40">
                <input type="text"
                className="form-control " name=""/>
                <button className="btn-primary">Search</button>
            </div>
        )
    }
}
