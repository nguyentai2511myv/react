import React, { Component } from 'react';

export default class Addlist extends Component {
  constructor(props){
    let randomstring = require("randomstring");
    super(props)
    this.state = {
      id: randomstring.generate(),
      name_data : "",
      status : true,
    }
    this.hideAddList = this.hideAddList.bind(this)
    this.save = this.save.bind(this)
    this.getValue = this.getValue.bind(this)
    this.remove = this.remove.bind(this)
  }

  remove(){
    this.setState({name_data : "",
    status : true})
    this.hideAddList()
  }

  getValue(e){
    this.setState({
      [e.target.name] : e.target.value
    })
  }
  hideAddList(){
    this.props.style()
  }
  save(e){
    e = this.state
    let status_active = Boolean;
    if(e.status === "true"  || e.status === true){
      status_active = true
    }else{
      status_active = false
    }
    e.status = status_active;
    this.props.SaveValue(e)
    this.hideAddList()
    this.remove()
  }

  componentWillMount(){
    if(this.props.task){
      let task = this.props.task
      this.setState({id: task.id,name_data: task.name_data, status:task.status})
    }
  }

  componentWillReceiveProps(param){
    this.setState({id: param.task.id,name_data: param.task.name_data, status:param.task.status})
    if(!param.task){
      let randomstring = require("randomstring");
      this.setState({
        id: randomstring.generate(),
        name_data : "",
        status : true
      })
    }
  }

  render() {
      return (
          <div className="col-4">
              <h5 className="pad-15 text-center" >
                {this.props.task === "" ? "Add List" : "Update List"}
                <i className="fa fa-times-circle float-right" aria-hidden="true" onClick={this.hideAddList}></i>
              </h5>
              <div className="form-group">
                <label>Name:</label>
                <input type="text"
                  className="form-control"
                  name="name_data"
                  value={this.state.name_data}
                  onChange={this.getValue} />
              </div>
              <div className="form-group">
                <label>State:</label>
                <select className="form-control"
                  name="status"
                  value = {this.state.status}
                  onChange={this.getValue} >
                  <option value={true}>Active</option>
                  <option value={false}>No active</option>
                </select>
              </div>
              <button type="submit" className="btn btn-primary" onClick={this.save}>Save</button>
              <button type="button" className="btn btn-danger" onClick={this.remove}>Remove</button>
          </div>
      )
  }
}
